variable "redis_password" {
  type    = string
  default = null
}
variable "redis_replicas" {
  type    = number
  default = 3
}
