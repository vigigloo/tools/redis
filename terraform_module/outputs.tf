locals {
  fullname = length(regexall("redis", var.chart_name)) > 0 ? trimsuffix(substr(var.chart_name, 0, 63), "-") : trimsuffix(substr("${var.chart_name}-redis", 0, 63), "-")
}

output "hostname" {
  value      = "${local.fullname}-master.${var.namespace}.svc.cluster.local"
  depends_on = [helm_release.redis]
}
