resource "helm_release" "redis" {
  chart           = "redis"
  repository      = "https://charts.bitnami.com/bitnami"
  name            = var.chart_name
  namespace       = var.namespace
  version         = var.chart_version
  force_update    = var.helm_force_update
  recreate_pods   = var.helm_recreate_pods
  cleanup_on_fail = var.helm_cleanup_on_fail
  max_history     = var.helm_max_history

  values = concat([
    file("${path.module}/redis.yaml"),
  ], var.values)

  dynamic "set" {
    for_each = var.redis_password == null ? [] : [var.redis_password]
    content {
      name  = "auth.password"
      value = var.redis_password
    }
  }
  dynamic "set" {
    for_each = var.redis_replicas == null ? [] : [var.redis_replicas]
    content {
      name  = "replica.replicaCount"
      value = var.redis_replicas
    }
  }

  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "master.resources.limits.cpu"
      value = var.limits_cpu
    }
  }
  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "master.resources.limits.memory"
      value = var.limits_memory
    }
  }
  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "master.resources.requests.cpu"
      value = var.requests_cpu
    }
  }
  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "master.resources.requests.memory"
      value = var.requests_memory
    }
  }
  dynamic "set" {
    for_each = var.limits_cpu == null ? [] : [var.limits_cpu]
    content {
      name  = "replica.resources.limits.cpu"
      value = var.limits_cpu
    }
  }
  dynamic "set" {
    for_each = var.limits_memory == null ? [] : [var.limits_memory]
    content {
      name  = "replica.resources.limits.memory"
      value = var.limits_memory
    }
  }
  dynamic "set" {
    for_each = var.requests_cpu == null ? [] : [var.requests_cpu]
    content {
      name  = "replica.resources.requests.cpu"
      value = var.requests_cpu
    }
  }
  dynamic "set" {
    for_each = var.requests_memory == null ? [] : [var.requests_memory]
    content {
      name  = "replica.resources.requests.memory"
      value = var.requests_memory
    }
  }
}
